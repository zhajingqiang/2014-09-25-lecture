#class are namespaces
class MyFunctions:
	def exp(self):
		return 0

ff=MyFunctions()
print ff.exp()

#magic functions - constructor
class Atom:
	def __init__(self, *param):
		self.atno=param[0]
		self.position=param[1:4]
		self.symbol={2:"He"}
	def getSymbol(self):
		return self.symbol[self.atno]  # index dictionary
	def __repr__(self):

		#formating values into decimals
		return "atno:%d, position:(%d,%d,%d)"%(self.atno,self.position[0],self.position[1],self.position[2])

at=Atom(2,2,3,4)
print at.position
print at.getSymbol
print at

#class container
class Molecule:
	def __init__(self,name='Generic'):   #default assignment
		self.name=name
		self.atomlist=[]  #type of the list is unknown
	def addAtom(self,atom):
		self.atomlist.append(atom)
	def __repr__(self):
		str='This is a molecule with name %s\n' %self.name
		str+='It has %d atoms\n'%len(self.atomlist)
		for atom in self.atomlist:
			print atom
		return str
mol=Molecule("Water")
at=Atom(8,0,0,0)
mol.addAtom(at)
mol.addAtom(Atom(1,0,0,1))
print mol

#inheritance
class QM_Molecule(Molecule):
	def __init__(self,name='Generic',basis=Atom(1,0,0,1))
	self.basis=[]
	self.basis.append(basis)
	Molecule.__init__(self,name)
	def setBasis(self):
		#self.basis=[]
		for atom in self.atomlist:
			self.basis.append(atom)

qmol=QM_Molecule("Hydrogen")
qmol.addAtom(Atom(1,0,0,1))
qmol.addAtom(Atom(2,0,0,1))
qmol.setBasis()




